# webcrawler
The web crawler has following classes:

**HtmlParser**: this class contains code related to parsing of the html page. The library Jsoup is used for parsing web pages. All the methods are static methods.

**WebCrawler**: this class contains the web crawler's functionality. The public crawl method is the entry point for crawling the webpage. Also, there is a max_depth instance variable to determine the level upto which the parsing needs to be done.

#Running the project
To run the project simply run the MainApp class as java application.
