package com.demo.webcrawler.parser;

import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.util.Set;

import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;

@RunWith(JUnit4ClassRunner.class)
public class HtmlParserTest {

	@Test
	public void testgetReferencedLinks() throws MalformedURLException {
		String path = "https://www.google.com/";
		Set<String> links = HtmlParser.getReferencedLinks(path, HtmlParser.getDomain(path));

		assertTrue(links.size() > 0);
	}

	@Test
	public void testgetReferencedLinksDomainDifferent() throws MalformedURLException {
		String path = "https://www.google.com/";
		Set<String> links = HtmlParser.getReferencedLinks(path, "gg");

		assertTrue(links.size() == 0);
	}
	
	@Test
	public void testgetImages() throws MalformedURLException {
		String path = "https://www.google.com/";
		Set<String> links = HtmlParser.getImages(path, HtmlParser.getDomain(path));

		assertTrue(links.size() > 0);
	}

	@Test
	public void testgetImagesDomainDifferent() throws MalformedURLException {
		String path = "https://www.google.com/";
		Set<String> links = HtmlParser.getImages(path, "gg");

		assertTrue(links.size() == 0);
	}
	

}
