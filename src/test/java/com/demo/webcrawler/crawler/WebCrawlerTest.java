package com.demo.webcrawler.crawler;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;

@RunWith(JUnit4ClassRunner.class)
public class WebCrawlerTest {

	private WebCrawler webCrawler;
	
	@Before
	public void setup() {
		webCrawler = new WebCrawler(0);
	}
	
	@Test
	public void testCrawl() {
		webCrawler.crawl("https://www.google.com/");
		assertTrue(webCrawler.getLinks().size() > 0);
		assertTrue(webCrawler.getImages().size() > 0);
	}
}
