package com.demo.webcrawler.crawler;

import java.net.MalformedURLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.stream.Collectors;

import com.demo.webcrawler.parser.HtmlParser;

/**
 * the webcrawler class
 *
 */
public class WebCrawler {

	/**
	 * the maximum depth upto which the pages need to be recursively scanned.
	 */
	private final int MAX_DEPTH;
	private Set<String> links = new HashSet<>();
	private Set<String> images = new HashSet<>();

	public WebCrawler(int maxDepth) {
		this.MAX_DEPTH = maxDepth;
	}

	public void crawl(String url) {
		final String domain = extractDomain(url);
		crawl(url, 0, domain);

	}

	/**
	 * 
	 * @param url
	 * @return
	 */
	private static String extractDomain(String url) {
		try {
			String domain = HtmlParser.getDomain(url);
			return domain;
		} catch (MalformedURLException e) {
			System.out.println("url is malformed");
			return null;
		}
	}

	/**
	 * method to recursively parse the pages
	 * 
	 * @param url    url of the current page to be scanned
	 * @param depth  the depth of the current page
	 * @param domain the domain of the links and images to be returned.
	 */
	private void crawl(String url, int depth, String domain) {
		if (!links.contains(url) && depth <= MAX_DEPTH) {
			depth++;
			// populating images
			images.addAll(HtmlParser.getImages(url, domain).stream().collect(Collectors.toSet()));
			// getting links
			Set<String> linksFetched = HtmlParser.getReferencedLinks(url, domain);
			for (String link : linksFetched) {
				links.add(link);
				crawl(link, depth, domain);
			}

		}
	}

	public Set<String> getLinks() {
		return links;
	}

	public Set<String> getImages() {
		return images;
	}

}
