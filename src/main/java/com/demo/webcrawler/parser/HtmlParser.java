package com.demo.webcrawler.parser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * utility class to parse the document
 *
 */
public class HtmlParser {

	/**
	 * the method returns the links that are referenced in the given web page, which
	 * are part of the same domain
	 * 
	 * @param url    url of the web page to be parsed
	 * @param domain the domain of the links to be returned.
	 * @return the set of links
	 */

	public static Set<String> getReferencedLinks(String url, final String domain) {
		// getting the html page
		try {
			Document document = Jsoup.connect(url).get();
			return document.select("a").stream().map(element -> element.absUrl("href"))
					.filter(a -> a != null && a.length() > 0).filter(a -> a.contains(domain))
					.collect(Collectors.toSet());
		} catch (IOException e) {
			System.out.println("web page parsing failed");
		}
		return Collections.emptySet();
	}

	/**
	 * the method returns the images referenced from the web page, which belong to
	 * the same domain
	 * 
	 * @param url    web page to be parsed
	 * @param domain the domain of the images to be returned
	 * @return the set of images
	 */
	public static Set<String> getImages(String url, final String domain) {
		// getting the html page
		try {
			Document document = Jsoup.connect(url).get();
			return document.select("img").parallelStream().map(element -> element.absUrl("src"))
					.filter(e -> e.contains(domain)).collect(Collectors.toSet());
		} catch (IOException e) {
			System.out.println("web page parsing failed");
		}
		return Collections.emptySet();
	}

	/**
	 * the method to get the domain of the given url
	 * 
	 * @param path the url of the web page
	 * @return the domain
	 * @throws MalformedURLException
	 */
	public static String getDomain(String path) throws MalformedURLException {
		URL url = new URL(path);
		return url.getHost();

	}

}
