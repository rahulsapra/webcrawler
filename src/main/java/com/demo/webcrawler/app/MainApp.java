package com.demo.webcrawler.app;

import java.net.MalformedURLException;

import com.demo.webcrawler.crawler.WebCrawler;

public class MainApp {
	public static void main(String[] args) throws MalformedURLException {
		WebCrawler webCrawler = new WebCrawler(1);

		webCrawler.crawl("https://www.tutorialspoint.com/jsoup/");
		System.out.println("images found: " + webCrawler.getImages().size());
		System.out.println("links found: " + webCrawler.getLinks().size());
	}
}
